#[macro_use]
extern crate lazy_static;

mod libs;

use crate::libs::task::po18_utils::{
    detect_download_type, extract_book_id, extract_with_chapter_id, DownloadType, Po18Task,
};
use console::{style, Term};
use dialoguer::Input;
use futures_retry::{FutureRetry, RetryPolicy};
use indicatif::{ProgressBar, ProgressStyle};
use std::fs::OpenOptions;
use std::path::Path;
use tokio::time::Duration;

async fn login() -> Result<Po18Task, String> {
    let input_username = Input::<String>::new()
        .with_prompt("请输入用户名")
        .allow_empty(false)
        .interact()
        .unwrap();

    let input_password = Input::<String>::new()
        .with_prompt("请输入密码")
        .allow_empty(false)
        .interact()
        .unwrap();

    Term::stdout().clear_screen().unwrap();
    println!("{}", style("正在登陆 po18...").bold().green());

    let po18 = Po18Task::new(input_username, input_password);

    match po18.login().await {
        true => Ok(po18),
        false => Err("Login failed".to_string()),
    }
}

fn login_retry_policy(_: String) -> RetryPolicy<bool> {
    println!("{}", style("登陆失败，请重试").bold().red());
    RetryPolicy::WaitRetry(Duration::from_secs(1))
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let (po18_instant, _) = FutureRetry::new(move || login(), login_retry_policy)
        .await
        .unwrap();

    Term::stdout().clear_screen().unwrap();
    let download_list_string = Input::<String>::new()
        .with_prompt("请输入下载信息，可输入多个 小说ID、小说链接、单个章节链接。使用空格隔开")
        .allow_empty(false)
        .interact()?;

    let download_list_raw = download_list_string
        .split(" ")
        .filter_map(|link| {
            let download_type = detect_download_type(link);
            match download_type {
                Some(dl_type) => match dl_type {
                    DownloadType::BookId => {
                        Some((["https://www.po18.tw/books/", link].concat(), dl_type))
                    }
                    DownloadType::Book => Some((link.to_string(), dl_type)),
                    DownloadType::ChapterOnly => Some((link.to_string(), dl_type)),
                },
                _ => None,
            }
        })
        .collect::<Vec<(String, DownloadType)>>();

    Term::stdout().clear_screen().unwrap();

    let mut current_progress = 0;
    let total_downloads = download_list_raw.len();
    for (download_link, download_type) in download_list_raw {
        current_progress += 1;

        let file_name: String = match download_type {
            DownloadType::ChapterOnly => {
                let (book_id, chapter_id) = extract_with_chapter_id(&download_link).unwrap();
                [book_id, "-", chapter_id, ".txt"].concat()
            }
            _ => {
                let book_id = extract_book_id(&download_link).unwrap();
                [book_id, ".txt"].concat()
            }
        };

        println!(
            "{} {} 到 {} ({}/{})",
            style("正在下载").bold().on_green().white(),
            download_link,
            style(&file_name).bold().on_blue().white(),
            current_progress,
            total_downloads
        );

        let article_list = po18_instant.download_article_list(&download_link).await;
        let progress_bar = ProgressBar::new(article_list.len() as u64);
        progress_bar.set_style(
            ProgressStyle::default_bar()
                .template(
                    "{spinner:.green} [{elapsed_precise}/{eta_precise}] [{bar:50.cyan/blue}] {pos}/{len} {msg}",
                )
                .progress_chars("#>-"),
        );

        let mut file_handle = OpenOptions::new()
            .write(true)
            .create(true)
            .open(&Path::new(&file_name))
            .expect("文件创建失败");

        match download_type {
            DownloadType::ChapterOnly => {
                po18_instant
                    .download_article(
                        (
                            "".to_string(),
                            download_link.replace("https://www.po18.tw", ""),
                        ),
                        &mut file_handle,
                    )
                    .await;
            }
            _ => {
                for chapters in article_list {
                    progress_bar.set_message(&chapters.0);
                    progress_bar.inc(1);
                    po18_instant
                        .download_article(chapters, &mut file_handle)
                        .await
                }

                progress_bar.finish();
            }
        };

        Term::stdout().clear_screen().unwrap();
    }

    println!("{}", style("下载完成").bold().on_green());
    Input::<String>::new()
        .with_prompt("")
        .allow_empty(true)
        .interact()
        .unwrap();

    Result::Ok(())
}
