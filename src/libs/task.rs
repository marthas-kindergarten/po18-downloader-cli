pub mod po18_utils {
    use futures_retry::{FutureRetry, RetryPolicy};
    use regex::Regex;
    use soup::{NodeExt, QueryBuilderExt, Soup};
    use std::collections::HashMap;
    use std::fs::File;
    use std::io::prelude::*;
    use tokio::time::Duration;

    static ADULT_CONSENT_URL: &str = "https://www.po18.tw/site/alarm";
    static PO18_MAIN_SITE: &str = "https://www.po18.tw";
    static PO18_LOGIN_CHECKPOINT: &str = "https://www.po18.tw/panel/stock_manage/stocks";
    static FIREFOX_USER_AGENT: &str =
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:85.0) Gecko/20100101 Firefox/85.0";

    fn default_http_get_retry_policy(error: reqwest::Error) -> RetryPolicy<reqwest::Error> {
        match error.status() {
            Some(reqwest::StatusCode::UNAUTHORIZED) => RetryPolicy::ForwardError(error),
            _ => RetryPolicy::WaitRetry(Duration::from_secs(3)),
        }
    }

    #[derive(Debug)]
    pub enum DownloadType {
        BookId,
        Book,
        ChapterOnly,
    }

    pub fn extract_book_id(link: &str) -> Option<&str> {
        lazy_static! {
            static ref REGEX_BOOK_ID_CAPTURE: Regex =
                Regex::new(r"https://www.po18.tw/books/(?P<book_id>\d+$)").unwrap();
        }

        REGEX_BOOK_ID_CAPTURE
            .captures(link)
            .and_then(|cap| cap.name("book_id").map(|book_id| book_id.as_str()))
    }

    pub fn extract_with_chapter_id(link: &str) -> Option<(&str, &str)> {
        lazy_static! {
            static ref REGEX_WITH_CHAPTER_ID: Regex = Regex::new(
                r"https://www.po18.tw/books/(?P<book_id>\d+)/articles/(?P<chapter_id>\d+)$"
            )
            .unwrap();
        }

        REGEX_WITH_CHAPTER_ID.captures(link).and_then(|cap| {
            let book_id = cap.name("book_id").map(|book_id| book_id.as_str());
            let chapter_id = cap.name("chapter_id").map(|chapter_id| chapter_id.as_str());

            if book_id.is_some() && chapter_id.is_some() {
                return Some((book_id.unwrap(), chapter_id.unwrap()));
            }

            None
        })
    }

    pub fn detect_download_type(link: &str) -> Option<DownloadType> {
        lazy_static! {
            static ref REGEX_FULL_NUMBER: Regex = Regex::new(r"^\d+$").unwrap();
            static ref REGEX_FULL_BOOK_LINK: Regex =
                Regex::new(r"https://www.po18.tw/books/\d+$").unwrap();
            static ref REGEX_FULL_CHAPTER_LINK: Regex =
                Regex::new(r"https://www.po18.tw/books/\d+/articles/\d+$").unwrap();
        }

        if REGEX_FULL_NUMBER.is_match(link) {
            return Some(DownloadType::BookId);
        }

        if REGEX_FULL_BOOK_LINK.is_match(link) {
            return Some(DownloadType::Book);
        }

        if REGEX_FULL_CHAPTER_LINK.is_match(link) {
            return Some(DownloadType::ChapterOnly);
        }

        None
    }

    pub struct Po18Task {
        username: String,
        password: String,
        client: reqwest::Client,
    }

    impl Po18Task {
        pub fn new(username: String, password: String) -> Self {
            Self {
                username,
                password,
                client: reqwest::Client::builder()
                    .cookie_store(true)
                    .user_agent(FIREFOX_USER_AGENT)
                    .timeout(Duration::from_secs(10))
                    .build()
                    .expect("Init error"),
            }
        }

        async fn is_logged_in(&self) -> bool {
            let (head_response, _) = FutureRetry::new(
                move || self.client.head(PO18_LOGIN_CHECKPOINT).send(),
                default_http_get_retry_policy,
            )
            .await
            .unwrap();

            head_response.url().to_string() == PO18_LOGIN_CHECKPOINT
        }

        async fn automatic_consent(&self, html: &str) {
            let soup = Soup::new(html);

            // 查找成年人认证 token
            let consent_token_input = soup
                .tag("input")
                .attr("name", "_po18rf-tk001")
                .find()
                .expect("Could not find consent token element");

            let consent_token = match consent_token_input.get("value") {
                None => panic!("Consent token do not exist"),
                Some(value) => value,
            };

            // 发送 “我已经18岁” 验证
            let mut consent_form: HashMap<&str, &str> = HashMap::new();
            consent_form.insert("_po18rf-tk001", &consent_token);

            FutureRetry::new(
                move || {
                    self.client
                        .post(ADULT_CONSENT_URL)
                        .form(&consent_form)
                        .send()
                },
                default_http_get_retry_policy,
            )
            .await
            .unwrap();
        }

        pub async fn login(&self) -> bool {
            let (ip_address_response, _) = FutureRetry::new(
                move || self.client.get("https://api.ipify.org").send(),
                default_http_get_retry_policy,
            )
            .await
            .map_err(|(e, _)| e)
            .expect("Retry failed");

            let ip_address = ip_address_response.text().await.unwrap();

            let mut login_post_data: HashMap<&str, &str> = HashMap::new();
            login_post_data.insert("account", &self.username);
            login_post_data.insert("pwd", &self.password);
            login_post_data.insert("client_ip", &ip_address);
            login_post_data.insert("remember_me", "1");
            login_post_data.insert("comefrom_id", "2");
            login_post_data.insert("owner", "COC");
            login_post_data.insert("front_events_id", "");
            login_post_data.insert("front_events_name", "");
            login_post_data.insert("url", &PO18_MAIN_SITE);

            let (response, _) = FutureRetry::new(
                move || {
                    self.client
                        .post("https://members.po18.tw/apps/login.php")
                        .form(&login_post_data)
                        .send()
                },
                default_http_get_retry_policy,
            )
            .await
            .map_err(|(e, _)| e)
            .expect("Retry failed");

            let url = response.url().to_string();
            let page_html = response.text().await.expect("Text conversion error");

            // Check if website is being redirected to consent page
            if &url == &ADULT_CONSENT_URL {
                self.automatic_consent(&page_html).await;
            }

            let logged = self.is_logged_in().await;

            logged
        }

        pub async fn download_article(&self, chapters: (String, String), txt_file: &mut File) {
            let (title, chapter) = chapters;
            let chapter_referer_url: String = ["https://www.po18.tw", &chapter].concat();

            let chapter_content_url: String = [
                "https://www.po18.tw",
                &chapter.replace("articles", "articlescontent"),
            ]
            .concat();

            let (response, _) = FutureRetry::new(
                move || {
                    self.client
                        .get(&chapter_content_url)
                        .header("Referer", &chapter_referer_url)
                        .send()
                },
                default_http_get_retry_policy,
            )
            .await
            .map_err(|(e, _)| e)
            .expect("Retry failed");

            let text = response.text().await.expect("Text conversion failed");

            let soup = soup::Soup::new(&text);
            let paragraphs = soup
                .tag("p")
                .find_all()
                .map(|p| String::from(p.text().trim_end()))
                .filter(|p| p.len() > 0)
                .collect::<Vec<_>>();

            txt_file
                .write(String::from([&title, "\r\n"].concat()).as_bytes())
                .unwrap();

            for paragraph in paragraphs {
                txt_file.write(paragraph.as_bytes()).unwrap();
                txt_file.write(b"\r\n").unwrap();
                txt_file.flush().unwrap();
            }

            txt_file.write(b"\r\n\r\n\r\n  ========  \r\n\r\n").unwrap();
        }

        pub async fn download_article_list(&self, url: &str) -> Vec<(String, String)> {
            let mut page_number: i8 = 1;
            let mut last_list_page: bool = false;
            let mut article_list: Vec<(String, String)> = vec![];

            while !last_list_page {
                let article_list_url: String =
                    [url, "/articles?page=", &page_number.to_string()].concat();

                let (article_response, _) = FutureRetry::new(
                    move || self.client.get(&article_list_url).send(),
                    default_http_get_retry_policy,
                )
                .await
                .map_err(|e| e)
                .expect("Retry failed");

                let article_html: String = article_response
                    .text()
                    .await
                    .expect("Text conversion failed");

                let soup = soup::Soup::new(&article_html);
                let pager_target = soup.class("page").find();

                last_list_page = match pager_target {
                    Some(pager) => {
                        let current_link = pager.tag("span").find();

                        match current_link {
                            Some(span) => span.text() == ">",
                            None => false,
                        }
                    }
                    None => true,
                };

                let chapters: Vec<(String, String)> =
                    match soup.tag("div").class("list-view").find() {
                        Some(list_view) => list_view
                            .tag("div")
                            .attr_name("data-key")
                            .find_all()
                            .map(|div| {
                                (
                                    match div.class("l_chaptname").find() {
                                        Some(div) => div.text(),
                                        None => String::from(""),
                                    },
                                    match div.tag("a").find() {
                                        Some(a) => {
                                            let link = a.get("href").unwrap_or(String::from(""));
                                            if link.starts_with("/books") {
                                                link
                                            } else {
                                                String::from("")
                                            }
                                        }
                                        None => String::from(""),
                                    },
                                )
                            })
                            .filter(|(chapter_name, chapter_link)| {
                                chapter_name != "" && chapter_link != ""
                            })
                            .collect::<Vec<_>>(),
                        None => {
                            vec![]
                        }
                    };

                article_list.extend(chapters);

                page_number += 1;
            }

            article_list
        }
    }
}
