# po18.tw 小说下载器 rust 版

本下载器处于 beta 状态

## 功能 checklist

- [x] 自动登陆
- [x] 自动成人认证
- [x] 章节识别
- [x] 保存为文件
- [ ] 错误重试
- [ ] 断点续传/更新下载
- [ ] 保存登陆状态

## 使用方法

1. 双击 exe 文件
2. 按照提示输入 账号，密码以及小说ID，回车确认
3. 自动下载所有内容到 小说ID.txt 文件中

## 使用方法2

1. 右键空白处，点击在此处打开命令行提示窗口 或 在此处打开 powershell 窗口
2. 输入 .\po18rust.exe 回车
3. 重复方法1中的2，3步骤

和使用方法1的不同，由于本程序目前会在遇到错误时直接崩溃，**为了能够知道是否下载完成而不是因为错误退出，使用方法2启动程序界面可以保留程序输出的信息。**

## 如何构建源码

本程序使用 rust 编写，需要在系统中安装 [rust 编译器](rust-lang.org/)

构建生产环境包 `cargo build --release`

构建调试包 `cargo build`

### 包大小优化

#### cargo.toml

- [x] LTO
- [x] opt-level
- [x] 删除 panic 时的栈回滚能力

#### upx 压缩

面向现代平台编译时可以使用 upx 提供的 lzma 算法对程序进行进一步的压缩

`upx --best --lamz target/release/po18rust`

#### 删除调试符号 (Linux & MacOS)

使用 strip 去除生产环境包中的调试符号

`strip target/release/po18rust`

### 交叉编译

#### MacOS 下编译  Win32 / Linux 程序

编译 Win32 需要使用连接器：`x86_64-w64-mingw32-gcc` 

可以使用brew 直接安装：`brew install mingw-w64`

编译时使用指定连接器：`RUSTFLAGS="-C linker=/usr/local/Cellar/mingw-w64/bin/x86_64-w64-mingw32-gcc" cargo build --release`



编译 Linux 需要使用连接器：`x86_64-linux-musl-gcc`